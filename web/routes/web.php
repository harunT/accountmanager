<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get("deneme", function(){
  return response(["msg" => "harun"], 200);
});
Route::get('/home', 'HomeController@index')->name('home');
Route::group([ 'prefix' => 'api'], function() {
  Route::post("login", "UsersController@login");
  Route::post("register", "UsersController@register");
  Route::post("checkAuth", "UsersController@checkAuth");
  Route::post("accounts", "AccountsController@get");
  Route::post("accounts/{id}", "AccountsController@get");
  Route::post("account/add", "AccountsController@createAccount");
  Route::post("account/update", "AccountsController@updateAccount");
  Route::post("account/delete", "AccountsController@deleteAccount");
  Route::post("account/check", "AccountsController@checkAccount");
  Route::post("reset_password", "AccountsController@resetPassword");
});

Route::get("pass_reset/{token}", "UsersController@getResetPassword")->name("reset");
Route::post("pass_reset", "UsersController@postResetPassword")->name("post_reset");

Route::get("heyreset", function(){
    $pass = twoWayEncript("J39QVTPY", "OAIifNDNpLfsYbf79LgKpQ==");
    echo $pass;
    echo "Dec: " . twoWayDecrypt("BHOLP1DkfQjzu/Fw76cDqw==", "OAIifNDNpLfsYbf79LgKpQ==");
});

