<?php
    use App\User;


    function getUserByHash($hash) {
    return (!$hash ? null : User::where("token", $hash)->first());
    }

    function decryptWithSecret($text, $secret=null){
      if(!$secret)
          $secret = env("SECRET");

      return openssl_decrypt($text, env("METHOD"),$secret,0,env("IV"));
    }

    function encryptWithSecret($text, $secret=null){
        if(!$secret)
            $secret = env("SECRET");

        return openssl_encrypt($text, env("METHOD"),$secret,0,env("IV"));
    }

    function twoWayEncript($text, $user_password){
      $secret = decryptWithSecret($user_password);

      return encryptWithSecret($text, $secret);
    }

    function twoWayDecrypt($text, $user_password){
      $secret = decryptWithSecret($user_password);

      return decryptWithSecret($text, $secret);
    }

    function refreshUserAccountPasswords($user, $newPassword){
      $accounts = $user->accounts;

      foreach($accounts as $account){
          $account->site_password = encryptWithSecret(twoWayDecrypt($account->site_password, $user->password), $newPassword);
          $account->save();
      }
    }
?>
