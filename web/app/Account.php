<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    protected $table = "user_account";
    protected $fillable = [
        'user_id', 'alias', 'site', 'site_email', 'site_password'
    ];

    function user()
    {
        return $this->belongsTo("App\User");
    }
}
