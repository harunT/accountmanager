<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;


use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

class UsersController extends Controller
{
    function login(Request $request)
    {
        if(($user = User::where(["email" => $request->email,"password" =>
            openssl_encrypt($request->password, env("METHOD"), env("SECRET"), 0, env("IV"))
        ])->first())){

            $better_token = md5(uniqid(mt_rand(), true) . $request->password);
            $user->token = $better_token;
            $user->save();

            return [
                "status" => 200,
                "token" => $user->token
            ];
        }else{
            return [
                "status" => 500,
                "error_message" => "Girilen bilgilere ait kullanıcı bulunamadı."
            ];
        }
    }

    function register(Request $request)
    {

        $request["password"] = openssl_encrypt($request->password,env("METHOD"),env("SECRET"),0,env("IV"));

        try{
            if(User::create($request->all()))
            {
                return [
                    "status" => 200
                ];
            }else{
                return ["status" => 500, "error_message" => "Kullanıcı kaydedilemedi."];
            }
        }catch (\Exception $e){
            return ["status" => 500,"error_message" => "Girilen mail adresi zaten kayıtlı."];
        }
    }

    function checkAuth(Request $request){
      return ["status" => 200, "user" => User::where("token", $request["token"])->first()];
    }

    function getResetPassword($token){
        if(User::where("remember_token", $token)->first())
            return view("auth.passwords.reset", ["token" => $token]);
        return "Error: Invalid Request!";
    }

    function postResetPassword(Request $request){
        $this->validate($request, [
            "email" => "required",
            "password" => "required|confirmed|min:8"
        ]);

        $user = User::where("remember_token", $request->remember_token)->first();
        if($user->email != $request->email){
            return redirect()->back()->withErrors("Email geçerli değil.");
        }

        refreshUserAccountPasswords($user, $request->password);

        $user->password = openssl_encrypt($request->password, env("METHOD"), env("SECRET"),0, env("IV"));
        $user->remember_token = null;
        $user->save();

        return "<html><body><h2>Şifreniz Başarıyla Değiştirildi</h2></body></html>";
    }

}
