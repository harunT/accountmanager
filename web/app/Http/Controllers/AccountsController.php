<?php

namespace App\Http\Controllers;

use App\Account;
use Illuminate\Http\Request;
use App\User;

class AccountsController extends Controller
{
    public $user;

    function __construct(Request $request){
       $this->user = $request["user"];
    }

    function get(Request $request, $id = null)
    {
        if($id == null)
        {
            $accounts = $this->user->accounts()->where(function($query) use ($request){
              $query->where("site", "like", "%". $request["site"] . "%")
                ->orWhere("alias", "like", $request["site"] . "%");
            })->get()->toArray();


            $accounts = array_map(function ($param){
                $param["site_password"] = twoWayDecrypt($param["site_password"], $this->user->password);

                return $param;

                }, $accounts);

            return ["status" => 200, "length" => count($accounts), "accounts" => $accounts];


        }else{
            $account = $this->user->accounts()->find($id)->toArray();
            $account["site_password"] = twoWayDecrypt($account["site_password"], $this->user->password);

            return $account;
        }

    }

    function createAccount(Request $request)
    {
        $request["site_password"] = twoWayEncript($request["site_password"], $this->user->password);

        if($account = Account::create($request->all()))
        {
               return [
                   "status" => 200,
                   "id" => $account->id
               ];
        }else{
            return [
                "status" => 500
            ];
        }
    }

    function updateAccount(Request $request)
    {

        $account = null;
        if($request->id){
          $account = $this->user->accounts()->find($request->id);
        }else{
          $account = $this->user->accounts()
          ->where("site", $request->site)
          ->where("site_email", $request->site_email)->first();
        }

        if(!$account){
            return [
                "status" => 500,
                "error_msg" => "Ilgili hesap bulunamadi.",
            ];
        }

        $request["site_password"] = twoWayEncript($request->site_password, $this->user->password);

        if($account->update($request->all()))
        {
            return [
                "status" => 200,
                "id" => $account->id
            ];
        }else{

            return [
                "status" => 500
            ];
        }
    }


    function deleteAccount(Request $request)
    {
        $accountBul = $this->user->accounts()->find($request->id);

        if(!$accountBul){
            return [
                "status" => 500,
                "error_message" => "Ilgili hesap bulunamadi.",
            ];
        }

        $this->user->accounts()->find($request->id)->delete();
        return ["status" => 200];
    }

    function checkAccount(Request $request){
      $account = $this->user->accounts()
      ->where("site", parse_url($request->site)["host"])
      ->where("site_email", $request->site_email)->first();

      $dec_password = ($account != null ? twoWayDecrypt($account->site_password, $this->user->password) : null);
      return ["status" => 200, "account_exists" => $account != null, "account" => $account, "dec_password" => $dec_password];
    }

    function resetPassword(Request $request){
      $email = $request["email"];
      $user = User::where("email", $email)->first();

      if(!$user){
        return ["status" => 500, "error_message" => "Girilen emaile ait kullanıcı bulunamadı."];
      }

      $user->remember_token = str_random(32);
      $user->save();

      \Mail::send("email", ["link" => route("reset", $user->remember_token)], function($m) use($email){
        $m->from("accountmanagerteam@live.com", "From");
        $m->to($email, "To");
      });

      return ["status" => 200];
    }
}
