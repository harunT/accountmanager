<?php

namespace App\Http\Middleware;

use Closure;

class TokenResolverMiddleware
{

  private $exceptions = [
    "/api/register",
    "/api/login",
    "/api/checkAuth",
    "/deneme",
    "/api/reset_password"
  ];
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $uri = $_SERVER["REQUEST_URI"];
        if(!in_array($uri, $this->exceptions) && strpos($uri, "reset") == false ){
          $request["user"] = getUserByHash($request["token"]);

          if($request["user"]){
              $request["user_id"] = $request["user"]->id;
          }else{
            return response(json_encode(["status" => 500, "error_message" => "Gecersiz token.", "request" => $request]), 500);
          }
        }

        return $next($request);
    }
}
