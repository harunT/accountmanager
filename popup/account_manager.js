var login_btn = document.getElementById("login_btn");
var register_btn = document.getElementById("register_btn");
var refresh_btn = document.getElementById("refresh_button");
var copy_btn = document.getElementById("copy_button");
var save_btn = document.getElementById("save_btn");
var add_btn = document.getElementById("add_btn");
var reset_btn = document.getElementById("reset_password_btn");
var password_output = document.getElementById("password_output");
var length_range = $("#length_range");
var current_user;
var account_identifier = $("#account_email");
var account_password = $("#account_password");
var account_site = $("#account_site");
var account_alias = $("#account_alias");
var account_id = $("#account_id");
var base_url = "http://localhost:8000";

$(".togglePasswordButton").on("click", function(){
  var el = $("#" + $(this).attr("data-password"));
  if(el.attr("type") == "password")
    el.attr("type", "text");
  else
    el.attr("type", "password");
});

function areFieldsValid(bound){
  return validate(bound);
}

function getErrorMessages(){
  return getValidationErrors();
}

function performDefaultValidation(el){
  var validation_passed = areFieldsValid(el.id);
  var msg = (validation_passed ? "" : getErrorMessages());
  document.getElementById(el.getAttribute("data-error-wrapper")).innerHTML = msg;

  return validation_passed;
}

function getAccountToEdit(account){
  account_alias.val(account.getAttribute("data-alias"));
  account_site.text(account.getAttribute("data-site") + " için hesap detaylarınız.");
  account_identifier.val(account.getAttribute("data-site-email"));
  account_password.val(account.getAttribute("data-site-password"));
  account_id.val(account.getAttribute("data-id"));
  navigateTo("account_editor");
}
