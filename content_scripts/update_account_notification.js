var div = document.createElement("div");
div.id = "custom_notification";
var u = document.createElement("u");
u.textContent = window.location.hostname;
div.append(u);
div.append(" için üyelik detaylarını güncellememizi ister misiniz ?");

var btnSave = document.createElement("button");
btnSave.addEventListener("click", function(e){
  browser.runtime.sendMessage({
    "command": "updateAccount",
    "host" : window.location.hostname,
    "protocol": window.location.protocol
  });
  clearScreen();
});
btnSave.id = "notf_button_save";
btnSave.textContent = "Güncelle";
btnSave.style.marginLeft = "24px";

var btnCancel = document.createElement("button");
btnCancel.addEventListener("click", function(e){
  browser.runtime.sendMessage({
    "button": "btnCancel"
  });
  clearScreen();
});
btnCancel.id = "notf_button_cancel";
btnCancel.textContent = "Vazgeç";

var p = document.createElement("p");
var small = document.createElement("small");
p.append(small);
small.textContent = "Kalan Süre: 10 sn";

div.style.position = "absolute";
div.style.top = "50px";
div.style.right = "50px";
div.style.zIndex = "999999";
div.style.padding = "12px";
div.style.border = "1px solid gray";
div.style.backgroundColor = "white";
div.style.fontSize = "14px";
div.style.boxShadow = "0px 0px 3px 0px gray";

div.append(btnSave);
div.append(btnCancel);
div.append(p);
document.body.append(div);

var timer = 10;
var interval = setInterval(function(){
  small.textContent = "Kalan Süre: " + --timer + " sn.";
}, 1000);
setTimeout(clearScreen, 1000 * 11);

function clearScreen(){
  clearInterval(interval);
  small.textContent = "";
  div.style.display = "none";
}
