(function(){

  if(window.agenRun)
    return;
  window.agentRun = true;

  browser.runtime.onMessage.addListener(function(message, sender, sendResponse){
    sendResponse({host: window.location.host});
  });

})();
