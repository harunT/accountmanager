(function() {

  if (window.hasRun) {
    return;
  }
  window.hasRun = true;

  function autofillForm(data){
    var inputs;
    inputs =
      document.querySelectorAll
      ("input[name*=email]"
    + ", input[type=email]"
    + ", input[id*=email]"
    + ", input[name*=username]"
    + ", input[id*=username]"
    + ", input[name*=user]"
    + ", input[name*=kullanici]"
    + ", input[name*=Kullanici]"
    + ", input[name*=Sifre]"
    + ", input[name*=sifre]"
    + ", input[id*=kullanici]"
    + ", input[id*=Kullanici]"
    + ", input[id*=Sifre]"
    + ", input[id*=sifre]");

    if(inputs.length > 0){
        for(var n in inputs){
          inputs[n].value = data.email;
        }
    }

    var password_field = document.querySelector("input[type=password]");
    password_field.value = data.password;
  }

  browser.runtime.onMessage.addListener(function(message){
    if (message.command === "autofill") {
      autofillForm(message.data);
    }
  });

  function output(data){
    var str = "";
    for(var p in data){
      str += data[p] + "<br>";
    }

    document.body.textContent = str;
  }

})();
