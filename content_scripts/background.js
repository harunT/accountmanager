var showNotf = false, seen = false, notf_id = "my_notification";

function logURL(requestDetails) {
  var data;
  var emailRegex = /(email|username|user|kullanici|Kullanici)/;
  var passwordRegex = /(password|pass|passwd|sifre|Sifre)/;
  var email_found = false;
  var password_found = false;

  if(requestDetails.method === "POST"
    && requestDetails.requestBody
    && (data = requestDetails.requestBody.formData)){
      console.log(requestDetails);

      if(data.take_action){
        return;
      }

      for(var prop in data){
          if(emailRegex.test(prop)){
            for(var el in data[prop]){
              console.log("DataProp is: " + data[prop]);
              if(data[prop][el] != ""){
                localStorage.setItem("site-email", data[prop][el]);
                email_found = true;
                break;
              }
            }
          }

          if(passwordRegex.test(prop)){
            for(var el in data[prop]){
              if(data[prop][el] != ""){
                localStorage.setItem("site-password", data[prop][el]);
                password_found = true;
                break;
              }
            }
          }
      }

      if(showNotf = email_found && password_found){
        localStorage.setItem("origin-url", requestDetails.originUrl);
      }

    console.log("SHOW_NOTF:", showNotf);
    console.log("EMAIL:", localStorage.getItem("site-email"));
    console.log("PASSWORD:", localStorage.getItem("site-password"));
    console.log("ORIGIN_URL:", localStorage.getItem("origin-url"));
  }
}

function createNotification(details){
  if(showNotf){
    showNotf = false;
    var shouldExec = false;
    var script = "create_account_notification.js";

    XHRPost("account/check", {
      "token": localStorage.getItem("token"),
      "site": localStorage.getItem("origin-url"),
      "site_email": localStorage.getItem("site-email")
    }, function(xhrRes){
      console.log("XHRRES: " + xhrRes);
      xhrRes = JSON.parse(xhrRes);

      if(xhrRes.account_exists){
        if(xhrRes.dec_password != localStorage.getItem("site-password")){
          shouldExec = true;
          script = "update_account_notification.js";
        }
      }else{
        shouldExec = true;
      }

      if(shouldExec){
        browser.tabs.executeScript({
          file: "content_scripts/" + script
        });
      }
    });
  }
}

browser.webRequest.onBeforeRequest.addListener(
  logURL,
  {urls: ["<all_urls>"]},
  ["blocking", "requestBody"]
);

browser.webRequest.onResponseStarted.addListener(
  createNotification,
  {urls: ["<all_urls>"]}
);

browser.runtime.onMessage.addListener(function(msg){
  if(msg.command === "saveAccount"){
    XHRPost("account/add", {
      "token": localStorage.getItem("token"),
      "alias": msg.host,
      "site": msg.host,
      "site_email": localStorage.getItem("site-email"),
      "site_password": localStorage.getItem("site-password")},  function(x){console.log(x)});
  }else if(msg.command == "updateAccount"){
    console.log("Updata data", {
      "token": localStorage.getItem("token"),
      "site": msg.host,
      "site_email": localStorage.getItem("site-email"),
      "site_password": localStorage.getItem("site-password")
    });
    XHRPost("account/update", {
      "token": localStorage.getItem("token"),
      "site": msg.host,
      "site_email": localStorage.getItem("site-email"),
      "site_password": localStorage.getItem("site-password")
    }, function(xhrRes){
      console.log(JSON.parse(xhrRes).responseText);
    });
  }else if(msg.command == "agent"){
    console.log(msg.host);
  }
});

function XHRPost(uri, data, success){
  var xhr = new XMLHttpRequest();
  var url = "http://localhost:8000/api/"

  xhr.open("POST", url + uri);
  xhr.setRequestHeader("Content-type", "application/json");
  xhr.onreadystatechange = function(res) {
    if(xhr.readyState == XMLHttpRequest.DONE && xhr.status == 200) {
      if(success != undefined)
        success(xhr.responseText);
    }
  }

  xhr.send(JSON.stringify(data));
}
