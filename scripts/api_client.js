var remote = "http://localhost:8000/api/";

function getAccountsForCurrentSite(){
  var wrapper = $("#accounts_found");
  var html = "";
  browser.tabs.executeScript({file: "/content_scripts/agent.js"})
  .then(function(){
    browser.tabs.query({active: true, currentWindow: true})
      .then(function(tabs){
        browser.tabs.sendMessage(tabs[0].id, {
              command: "getHost",
        }).then(function(res){
          $.ajax({
              "type": "POST",
              "url": remote + "accounts",
              "data": {token: localStorage.getItem("token"), "site": res.host}
            }).then(function(res){
              if(res.status == 200){
                if(parseInt(res.length) > 0){
                  res.accounts.forEach(function(i){
                    html += accountDivHtmlBuilder(i);
                  });
                }else{
                  html = "Kayıtlı hesap bulunamadı.";
                }

                wrapper.html(html);
                bindListeners();
              }
            });
        });
      });
  });
}

function getAllAccounts(pattern){
  var wrapper = $("#all_accounts");
  var html = "";
  $.ajax({
      "type": "POST",
      "url": remote + "accounts",
      "data": {token: localStorage.getItem("token"), "site": pattern}
    }).then(function(res){
      if(res.status == 200){
        if(parseInt(res.length) > 0){
          res.accounts.forEach(function(i){
            html += accountDivHtmlBuilder(i);
          });
        }else{
          html = "Kayıtlı hesap bulunamadı.";
        }

        wrapper.html(html);
        bindListeners();
      }
    });
}

function accountDivHtmlBuilder(i){
  return ('<div class="account" id="' + i.id
  + '" data-id="' + i.id
  + '" data-alias="' + i.alias
  + '" data-site="' + i.site
  + '" data-user-id="' + i.user_id
  + '" data-site-email="' + i.site_email
  + '" data-site-password="' + i.site_password
  + '"><p>' + i.alias
  + '<span class="pull-right"><small><a class="edit_account_btn" data-account-id="' + i.id
  + '">Düzenle</a> <a class="delete_account_btn" data-account-id="' + i.id
  + '">Kaldır</a></small></span><br><small>' + i.site_email
  + '</small></p></div>');
}

function bindListeners(){
  //edit button listener
  $(".edit_account_btn").on("click", function(e){
    e.stopImmediatePropagation();
    getAccountToEdit(document.getElementById($(this)[0].getAttribute("data-account-id")));
  });

  //delete button listener
  $(".delete_account_btn").on("click", function(e){
    e.stopImmediatePropagation();
    if(confirm("Hesabı silmek istediğinize emin misiniz ?")){
      var id = $(this)[0].getAttribute("data-account-id");
      var res = handleDeleteAccount(id);
      if(res.responseJSON.status == 200){
        document.getElementById(id).style.display = "none";
        alert("Hesap başarı ile silindi");
      }else{
        alert(res.responseJSON.error_message);
      }
    }
  });

  //account_div listener
  $(".account").on("click", function(){
    var self = $(this)[0];
    var _data = {
      email: self.getAttribute("data-site-email"),
      password: self.getAttribute("data-site-password")
    };
    browser.tabs.executeScript({file: "/content_scripts/auto_fill.js"})
    .then(function(){
      browser.tabs.query({active: true, currentWindow: true})
        .then(function(tabs){
          browser.tabs.sendMessage(tabs[0].id, {
                command: "autofill",
                data: _data
          });
        })
        .catch(function(err){
          alert("TabQueryAlert: " + err);
        })
    })
    .catch(function(err){
      alert("ExecuteScriptAlert: " + err);
    });
  });
}

function handleLogin(data){
  return $.ajax({
    type: "POST",
    url: remote + "login",
    data: data,
    async: false
  });
}

function handleRegister(data){
  return $.ajax({
    type: "POST",
    url: remote + "register",
    data: data,
    async: false
  });
}

function handleAddAccount(data){
  return $.ajax({
    type: "POST",
    url: remote + "account/add",
    data: data,
    async: false
  });
}

function handleUpdateAccount(data){
  return $.ajax({
    type: "POST",
    url: remote + "account/update",
    data: data,
    async: false
  });
}

function handleDeleteAccount(id){
  return $.ajax({
    type: "POST",
    url: remote + "account/delete",
    data: {token: localStorage.getItem("token"), id: id},
    async: false
  });
}

function handleResetPassword(data){
    return $.ajax({
        type: "POST",
        url: remote + "reset_password",
        data: data,
        async: false
    });
}
