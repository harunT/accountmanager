var popup_history = [];
var unprotected_rotues = [
  "login_section",
  "register_section"
];
var topnav = $("#top_nav");
var auth = localStorage.getItem("auth");

function navigateTo(section_id){
  var current_section = $(".active_section");

  if(current_section.length > 0){
    popup_history.push(current_section.attr("id"));
  }

  $(".section").each(function(index){
    $(this).css("display", "none");
    if($(this).hasClass("active_section"))
      $(this).removeClass("active_section");
  });

  if(unprotected_rotues.indexOf(section_id) > -1)
    topnav.css("display", "none");
  else{
    topnav.css("display", "block");
  }

  switch(section_id){
      case "main_section":
        getAccountsForCurrentSite();
        break;
      case "vault_section":
        getAllAccounts("");
        break;
      case "password_section":
        document.getElementById("password_output").value = generatePassword();
        break;
      case "account_section":
        if(confirm("Çıkmak istediğinize emin misiniz ?")){
          localStorage.removeItem("auth");
          localStorage.removeItem("token");
          navigateTo("login_section");
        }
        break;
      default:
        console.log("default");
        break;
  }

  $("#" + section_id).css("display", "block").addClass("active_section");
}

function generatePasswordO(){
  var chars = "", password = "", el,
  options = {
    uppercase: "ABCDEFGHIJKLMNOPRSTUVYZXW",
    lowercase: "abcdefghijklmnoprstuvyzxw",
    digit: "0123456789",
    special_chars: "!@#$%^&"
  };

  for(var opt in options){
    el = $("#" + opt);
    chars += (el.prop("checked") ? options[opt] : "");
  }

  for(var i = 0, j = length_range.val(), m = chars.length; i < j; i++){
    password += chars.charAt(Math.floor(Math.random() * m));
  }

  return password;
}

function generatePassword(){
  var chars = "", password = "", el,
  options = {
    uppercase: "ABCDEFGHIJKLMNOPRSTUVYZXW",
    lowercase: "abcdefghijklmnoprstuvyzxw",
    digit: "0123456789",
    special_chars: "!@#$%^&"
  };

  var range = length_range.val(), criteria_count = 0, remainder, extra_chars = 0, total_range, loop = 0;

  for(var opt in options){
    el = $("#" + opt);
    if(el.prop("checked")){
      ++criteria_count;
    }
  }

  if((remainder = range % criteria_count)){
    console.log("Remainder",remainder);
    extra_chars = criteria_count - remainder;
  }
  total_range = parseInt(range) + extra_chars;
  loop = total_range / criteria_count;

  for(var opt in options){
    el = $("#" + opt);
    if(el.prop("checked")){
      for(var i = 0, j = loop, m = options[opt].length; i < j; i++){
        password += options[opt].charAt(Math.floor(Math.random() * m));
      }
    }
  }

  var step = 1, pos;
  while(step++ <= extra_chars){
    pos = step * (extra_chars-1);
    password = password.substring(0, pos) + password.substring(pos + 1, password.length);
  }

  return shuffle(password);
}

function shuffle(str){
    var a = str.split(""), n = a.length;

    for(var i = n - 1; i > 0; i--) {
        var j = Math.floor(Math.random() * (i + 1));
        var tmp = a[i];
        a[i] = a[j];
        a[j] = tmp;
    }
    return a.join("");
}

function navigateBack(){
  if(popup_history.length > 0)
    navigateTo(popup_history.pop());
  else
    return false;
}
