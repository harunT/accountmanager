var login_email = $("#login_form_email");
var login_password = $("#login_form_password");
var register_email = $("#register_form_email");
var register_password = $("#register_form_password");
var search_input = $("#account_search_input");

search_input.on("keyup", function(e){
  var val = $(this).val();
  getAllAccounts(val);
})

length_range.on("input", function(){
  $("#password_length").text($(this).val());
});

login_btn.addEventListener("click", function(){
  if(performDefaultValidation(login_btn)){
    var email = login_email.val();
    var password = login_password.val();
    var res = handleLogin({"email": email, "password": password});
    console.log(res.responseJSON);
    if(res.responseJSON.status == 200){
        localStorage.setItem("token", res.responseJSON.token);
        localStorage.setItem("auth", true);
        navigateTo("main_section");
    }else{
      alert(res.responseJSON.error_message);
    }
  }
});

register_btn.addEventListener("click", function(){
  if(performDefaultValidation(register_btn)){
    var email = register_email.val();
    var password = register_password.val();
    var res = handleRegister({"email": email, "password": password});

    if(res.responseJSON.status == 200){
        alert("Kaydınız oluşturuldu. Üye girişi yapın.");
        navigateTo("login_section");
    }else{
      alert(res.responseJSON.error_message);
    }
  }
});

save_btn.addEventListener("click", function(){
  if(performDefaultValidation(save_btn)){
    var fields = {
      //site: $("#add_account_site"),
      alias: $("#account_alias"),
      site_email: $("#account_email"),
      site_password: $("#account_password"),
      id: $("#account_id")
    };

    var data = {token: localStorage.getItem("token"), take_action: false};
    for(var f in fields){
      data[f] = fields[f][0].value;
    }
    var res = handleUpdateAccount(data);
    if(res.responseJSON.status == 200){
        alert("Hesap başarıyla güncellendi.");
        for(var f in fields){
          fields[f].val("");
        }
    }else{
      alert(res.responseJSON.error_message);
    }
  }
});

add_btn.addEventListener("click", function(){
  if(performDefaultValidation(add_btn)){
    var fields = {
      site: $("#add_account_site"),
      alias: $("#add_account_alias"),
      site_email: $("#add_account_email"),
      site_password: $("#add_account_password")
    };

    var data = {token: localStorage.getItem("token"), take_action: false};
    
    for(var f in fields){
      data[f] = fields[f][0].value;
    }
    var res = handleAddAccount(data);
    if(res.responseJSON.status == 200){
        alert("Hesap başarıyla eklendi.");
        for(var f in fields){
          fields[f].val("");
        }
    }else{
      alert(res.responseJSON.error_message);
    }
  }
});

reset_btn.addEventListener("click", function(){
    var res = handleResetPassword({email: $("#reset_password_email").val()});
    if(res.responseJSON.status == 200){
        alert("Sıfırlama linki email adresinize gönderildi.");
        navigateTo("login_section");
        for(var f in fields){
            fields[f].val("");
        }
    }else{
        alert(res.responseJSON.error_message);
    }
});

refresh_button.addEventListener("click", function(){
  password_output.value = generatePassword();
});

copy_button.addEventListener("click", function(){
  password_output.select();
  document.execCommand("copy");
});

$(".navigator").on("click", function(e){
  navigateTo($(this).attr("data-ref"));
});

$(".back").on("click", function(e){
  navigateBack();
});

var authUser = getAuthUser();
console.log(localStorage.getItem("token"));
if(!authUser && localStorage.getItem("auth") != "true"){
  localStorage.setItem("auth", false);
  navigateTo("login_section");
}else{
  localStorage.setItem("auth", true);
  navigateTo("main_section")
}
