var authUser, is_authenticated = false;

function getAuthUser(){
  var token = localStorage.getItem("token");
  if(!token){
    return false;
  }

  var res = getUserByToken(token);
  var authUser = (res.status == 200 ? (res.responseJSON.user == null ? false : res.responseJSON.user) : false);
  return authUser;
}

function getUserByToken(token){
  return $.ajax({
    type: "POST",
    url: remote + "checkAuth",
    data: {token: token},
    async: false
  });
}
