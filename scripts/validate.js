var validation_rules, validation_error_message = "<ul class='validation_errors'>";

function validate(bound){
  var fields = document.querySelectorAll('[data-bound="' + bound + '"]');
  var isValid = true;

  fields.forEach(function(el, i, l){
    if(el.hasAttribute("data-validate")){
      validation_rules = el.getAttribute("data-validate").split("|");
      validation_rules.forEach(function(e, j, li){
        if(!performValidation(el, e))
          isValid = false;
      })
    }
  });

  return isValid;
}

function getValidationErrors(){
  var copy = validation_error_message + "</ul>";
  validation_error_message = "<ul class='validation_errors'>";
  return copy;
}

function performValidation(element, rule){
  var data = element.value;
  var name = element.getAttribute("data-name");
  var parts = rule.split(":");

  switch(parts[0]){
    case "required":
      if(data.length == 0){
        validation_error_message += "<li class='error_message'>" + name + " boş bırakılamaz</li>";
        return false;
      }
      break;

    case "min_length":
      if(data.length < parts[1]){
        validation_error_message += "<li class='error_message'>" + name + " en az " + parts[1] + " karakterden oluşmalıdır.</li>";
        return false;
      }
      break;

    case "max_length":
      if(data.length > parts[1]){
        validation_error_message += "<li class='error_message'>" + name + " " + parts[1] + " karakterden uzun olamaz.</li>";
        return false;
      }
      break;

    case "numeric":
      if(!/^\d+\.?\d+$/.test(data)){
        validation_error_message += "<li class='error_message'>" + name + " yalnızca rakamlardan oluşmalıdır.</li>";
        return false;
      }
      break;

    case "min":
      var floatData = parseFloat(data);
      if(floatData != floatData){
        validation_error_message += "<li class='error_message'>" + name + " yalnızca rakamlardan oluşmalıdır.</li>";
        return false;
      }
      if(floatData < parseFloat(parts[1])){
        validation_error_message += "<li class='error_message'>" + name + " " + parts[1] + "'den küçük olamaz.</li>";
        return false;
      }
      break;

    case "max":
      var floatData = parseFloat(data);
      if(floatData != floatData){
        validation_error_message += "<li class='error_message'>" + name + " yalnızca rakamlardan oluşmalıdır.</li>";
        return false;
      }
      if(floatData > parseFloat(parts[1])){
        validation_error_message += "<li class='error_message'>" + name + " " + parts[1] + "'den büyük olamaz.</li>";
        return false;
      }
      break;

    case "confirmed":
      var corfirming_element = document.getElementById(parts[1]);
      if(!corfirming_element) return false;

      var corfirmation_data = corfirming_element.value;
      if(data.length > 1 && data != corfirmation_data){
        validation_error_message += "<li class='error_message'>Girilen " + name + "ler uyuşmuyor.</li>";
        return false;
      }
      break;
  }

  return true;
}
